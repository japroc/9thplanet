from __future__ import print_function, division, unicode_literals, absolute_import

import subprocess
import os
import re

RIGHT_READ = 'READ'
RIGHT_WRITE = 'WRITE'
RIGHT_DELETE = 'DELETE'

BUILTIN_USERS = ['Administrator', 'DefaultAccount', 'Guest', 'japroc', 'WDAGUtilityAccount']
BUILTIN_GROUPS = [
    'Access', 'Administrators', 'Backup', 'Cryptographic', 'Distributed', 
    'Event', 'Guests', 'Hyper-V', 'IIS_IUSRS', 'Network', 'Performance', 
    'Performance', 'Power', 'Remote', 'Remote', 'Replicator', 'System', 'Users'
]


def _runPowershellCommand(powershell_command):
    args = ['powershell', '-c', powershell_command]
    p = subprocess.Popen(args, stdout=subprocess.PIPE)
    out, err = p.communicate()
    if err:
        print('[!] runPowershellCommand. Error: {}'.format(err))
        exit(1)

    return out

# -------------
# --- Users ---
# -------------

def createUser(username):
    powershell_command = '$SecurePassword=ConvertTo-SecureString qwerty -asplaintext -force; New-LocalUser "{}" -Password $SecurePassword -FullName "{}" -Description "Description of this account."'.format(username, username)
    res = _runPowershellCommand(powershell_command)
    
    if 'New-LocalUser : User {} already exists.'.format(username) not in res:
        return True
    else:
        return False

def removeUser(username):
    powershell_command = 'Remove-LocalUser -Name "{}"'.format(username)
    res = _runPowershellCommand(powershell_command)
    if len(res) == 0:
        return True
    else:
        return False

def _getAllUsers():
    powershell_command = 'Get-LocalUser'
    res = _runPowershellCommand(powershell_command)
    lines_with_users = filter(lambda a: a, res.split('\r\n'))[2:]
    users = map(lambda line: line.split()[0], lines_with_users)
    return users

def getUsers():
    users = _getAllUsers()
    lab_users = filter(lambda user: user not in BUILTIN_USERS, users)
    return lab_users

# --------------
# --- Groups --- 
# --------------

def createGroup(groupname):
    powershell_command = 'New-LocalGroup -Name "{}"'.format(groupname)
    res = _runPowershellCommand(powershell_command)
    if 'already exists' in res:
        return False
    else:
        return True

def removeGroup(groupname):
    powershell_command = 'Remove-LocalGroup -Name "{}"'.format(groupname)
    res = _runPowershellCommand(powershell_command)
    if len(res) == 0:
        return True
    else:
        return False

def _getAllGroups():
    powershell_command = 'Get-LocalGroup'
    res = _runPowershellCommand(powershell_command)
    lines_with_groups = filter(lambda a: a, res.split('\r\n'))[2:]
    groups = map(lambda line: line.split()[0], lines_with_groups)
    return groups

def getGroups():
    groups = _getAllGroups()
    lab_groups = filter(lambda group: group not in BUILTIN_GROUPS, groups)
    return lab_groups

# -----------------------------
# --- User-Group Management ---
# -----------------------------

def addUserToGroup(username, groupname):
    powershell_command = 'Add-LocalGroupMember -Group "{}" -Member "{}"'.format(groupname, username)
    res = _runPowershellCommand(powershell_command)
    if len(res) == 0:
        return True
    else:
        return False

def removeUserFromGroup(username, groupname):
    powershell_command = 'Remove-LocalGroupMember -Group "{}" -Member "{}"'.format(groupname, username)
    res = _runPowershellCommand(powershell_command)
    if len(res) == 0:
        return True
    else:
        return False

# -----------
# --- ACL ---
# -----------

def addAcl(filepath, name, right):
    get_acl = '$Acl = Get-Acl "{}"'.format(filepath)
    new_ace = '$Ar = New-Object System.Security.AccessControl.FileSystemAccessRule ("{}","{}","Allow")'.format(name, right)
    acl_add_ace = '$Acl.AddAccessRule($Ar)'
    set_acl = 'Set-Acl "{}" $Acl'.format(filepath)

    powershell_commands = '; '.join([get_acl, new_ace, acl_add_ace, set_acl])
    res = _runPowershellCommand(powershell_commands)

    if len(res) == 0:
        return True
    else:
        return False
