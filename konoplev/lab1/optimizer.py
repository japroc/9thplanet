"""
    How to run powershell commands: https://stackoverflow.com/questions/18454653/run-powershell-command-from-command-prompt-no-ps1-script
"""
from __future__ import print_function, division, unicode_literals, absolute_import

import os
import time
import json
import random
from future.utils import iteritems

# from IPython import embed

import powershellapi as psapi


ACCESS_READ = 'r'
ACCESS_WRITE = 'w'
ACCESS_DELETE = 'd'
ACCESSES = [ACCESS_READ, ACCESS_WRITE, ACCESS_DELETE]


def optimize(users, files, rights):
    """

    input:
        (list) users: ['u1', 'u2', ...]
        (list) files: ['f1', 'f2', ...]
        (list of dicts) old_rights: [{'subject':'u1', 'subject_type':'user', 'access':'r', 'object':'f1', 'type':'allow'}, ...] 
    return:
        (dict) {
            'users': ['u1', 'u2'],
            'files': ['f1', 'f2'],
            'groups': {'g1':['u1', 'u2'], ...},
            'rights': [{'subject':'g1', 'subject_type':'group', 'access':'r', 'object':'f1', 'type':'allow'}, ...]
        }
    """
    groups = dict()
    new_rights = list()
    old_rights = rights[:]

    for file in files:
        rights_to_file = filter(lambda right: right.get('object') == file, old_rights)

        for access in ACCESSES:
            cur_access_rights = filter(lambda right: right.get('access') == access, rights_to_file)

            if len(cur_access_rights) < 2:
                continue

            new_group_name = 'g{}'.format(len(groups) + 1)
            new_group_users = map(lambda right: right.get('subject'), cur_access_rights)
            groups[new_group_name] = new_group_users

            new_rights.append({
                'subject': new_group_name,
                'subject_type': 'group',
                'access': access,
                'object': file,
                'type': 'allow'
            })

            for r in cur_access_rights:
                old_rights.remove(r)

    rights = new_rights + old_rights

    return {
        'users': users,
        'files': files,
        'groups': groups,
        'rights': rights
    }


def main():
    users = ['u1', 'u2', 'u3', 'u4', 'u5']
    files = ['f1', 'f2', 'f3', 'f4', 'f5']
    rights = [
        {'subject':'u1', 'subject_type':'user', 'access':'r', 'object':'f1', 'type':'allow'},
    ]

    start_time = time.time()
    res = optimize(users, files, rights)
    optimize_time = time.time()
    print('Optimized {} to {} ACE in {} seconds.\nResult: {}'.format(len(rights), len(res['rights']), optimize_time - start_time, json.dumps(res)))

    finished_time = time.time()
    print('Finished in {} seconds'.format(finished_time - start_time))

    return


def cleanup():
    # files
    fnames = os.listdir('stand')
    for fname in fnames:
        os.remove(os.path.join('stand', fname))
    # users
    for user in psapi.getUsers():
        psapi.removeUser(user)
    # groups
    for group in psapi.getGroups():
        psapi.removeGroup(group)


def do(user_cnt, file_cnt):
    cleanup()

    users = ['u{}'.format(i) for i in range(user_cnt)]
    files = ['f{}.txt'.format(i) for i in range(file_cnt)]
    rights = list()

    # randomly generate rights
    for u in users:
        for f in files:
            for a in ACCESSES:
                if random.randint(1, 3) == 1:
                    rights.append({'subject':u, 'subject_type':'user', 'access':a, 'object':f, 'type':'allow'})

    start_time = time.time()
    res = optimize(users, files, rights)
    optimize_time = time.time()
    print('Old rights:\n{}'.format(json.dumps(rights)))
    print('New rights:\n{}'.format(json.dumps(res['rights'])))
    print('Optimized {} to {} ACE in {} seconds.\nResult: {}'.format(len(rights), len(res['rights']), optimize_time - start_time, json.dumps(res)))

    for user in res['users']:
        psapi.createUser(user)
    for (gname, gusers) in iteritems(res['groups']):
        psapi.createGroup(gname)
        for guser in gusers:
            psapi.addUserToGroup(guser, gname)
    for fname in res['files']:
        filepath = os.path.join('stand', fname)
        fp = open(filepath, 'w')
        fp.write(fname)
        fp.close()
    for right in res['rights']:
        filepath = os.path.join('stand', right['object'])
        access = None
        if right['access'] == ACCESS_READ:
            access = psapi.RIGHT_READ
        if right['access'] == ACCESS_WRITE:
            access = psapi.RIGHT_WRITE
        if right['access'] == ACCESS_DELETE:
            access = psapi.RIGHT_DELETE
        psapi.addAcl(filepath, right['subject'], access)


    finished_time = time.time()
    print('Finished in {} seconds'.format(finished_time - start_time))

if __name__ == '__main__':
    main()
