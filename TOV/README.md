## TOV 4 - Исследование механизмов полиморфизма программного кода
[Смотри сюда](https://gitlab.com/japroc/9thplanet/tree/master/TOV/TOV4)

## TOV 3
[Смотри сюда](https://gitlab.com/japroc/9thplanet/blob/master/TOV/TOV_LAB3.md)

## Доп вопросы по TOV 1
### Вопрос
* От каких параметров зависти DPAPI'шная функция расшифрования. От SID и пароля (хэша пароля) точно. Зная SID и пароль, можно ли расшифровать на другом хосте? Может ли администратор сети, зная SID и пароль пользователя расшифровать этот файл.
* Здесь нужно определить, зависит ли DPAPI еще от каких-то параметров, помимо тех, что задаются при вызове функи  `CryptUnprotectData` ?
* В результате определить можно ли восстанавливать файл. Скорее всего ответ ДА
* Если да, то можно либо реализовать самому, либо найти скрипт/прогу, которая это делает.
* Если нет - объяснить почему.
### Ответ
===Про DPAPI===
1. ((https://www.nirsoft.net/utils/dpapi_data_decryptor.html DataProtectionDecryptor)) - тут есть описание. Проблема с хайвами - постоянно заняты, не могу скопировать.
2. ((https://cqureacademy.com/blog/windows-internals/black-hat-europe-2017 Black Hat Europe 2017. DPAPI and DPAPI-NG: Decryption Toolkit)) - почему-то не скачивается.
3. ((https://github.com/jordanbtucker/dpapick Тулза на питоне))

===DPAPI и питоновская тулза dpapick===
1. Скачать архив M2Crypto отсюда https://github.com/dsoprea/M2CryptoWindows и разархивировать
2. Добавиь в PYTHONPATH M2Crypto и DPAPI из dpapick
3. pip install python-registry
4. Тырим
  - логин
  - пароль
  - masterkey folder: `C:\Users\<username>\AppData\Roaming\Microsoft\Protect\<user_sid>`
  - chrome pass folder: `C:\Users\<username>\AppData\Local\Google\Chrome\User Data\Default\Login Data`
5. Запускаем вот так (dpapick\examples\chrome):
  `python chrome --sid <sid> --password 111 --masterkey C:\Users\<username>\Desktop\TOV2_WORKING\DOP\VICTIM\dop1\TOV1\<sid> --chrome "C:\Users\<username>\Desktop\TOV2_WORKING\DOP\VICTIM\dop1\TOV1\Login Data"`
## Доп вопросы по TOV 2
### Вопрос
* Определить что за PASSIVE_LEVEL? Это IRQL. Какие еще есть значения. Что они обозначают. Как их можно выставить?
* [Вики Линка](https://h.yandex-team.ru/?https%3A%2F%2Fru.wikipedia.org%2Fwiki%2FIRQL)
* [MSDN: What is IRQL?](https://blogs.msdn.microsoft.com/doronh/2010/02/02/what-is-irql/)
### Ответ
==Про IRQL==

IRQL - букв. уровень запроса прерывания.

Уровни:
* High (31)
* Power fail (30)
* IPI (29)
* Clock (28)
* Profile (27)
* Диапазон аппаратных прерываний, называемых Devices IRQL, или DIRQL (от 26 до 3)
* DPC/DISPATCH (2)
* APC (1)
* PASSIVE (0)

PASSIVE - обычные потоки.

PASSIVE - User threads and most kernel-mode operations

В нашем драйвере всегда прилетает PASSIVE.

Но вообще код с низшим приоритетом не может прервать код с более высоким приоритетом.

Кул стори:
```
Легендарная ошибка с сообщением IRQL_NOT_LESS_OR_EQUAL означает следующую ситуацию: драйвер или другой привилегированный код с IRQL >= DPC/DISPATCH обратился к отсутствующей в памяти странице, требуется вызов подсистемы, подгружающей страницы с диска, однако эта подсистема в соответствии с архитектурой Windows NT имеет IRQL меньше, чем DPC/DISPATCH. Следовательно, она не имеет права прерывать тот код, который вызвал ошибку страницы. В то же время привилегированный код не может продолжить выполнение, пока страница не будет загружена. Возникает логический тупик, который, собственно, и приводит к краху ОС.
```