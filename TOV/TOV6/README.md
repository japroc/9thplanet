### Driver Management
* Install: `"Right-click inf file" -> "Install"`
* Start: `sc start pass_through`
* Stop: `sc stop pass_through`
* Delete: `sc delete pass_through`

### Where
${LOCALAPPDATA}\\Google\\Chrome\\User Data\\Default\\Cookie
${USERPROFILE}\\Desktop\\Cookies

### Links
* https://www.winvistatips.com/threads/full-path-for-a-process.189204/
* https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/wdm/
* https://stackoverflow.com/questions/39348630/how-get-current-process-image-file-full-name-in-filter-driver