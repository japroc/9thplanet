#### Links
* https://security.stackexchange.com/questions/117905/what-fuzzy-hashing-algorithms-exist // stackoverflow question
* https://ssdeep-project.github.io/ssdeep/index.html // ssdeep
* https://documents.trendmicro.com/assets/wp/wp-locality-sensitive-hash.pdf // tlsh
* http://roussev.net/sdhash/tutorial/sdhash-tutorial.pdf // sdhash
* https://github.com/scrapinghub/python-simhash // simhash

#### Че делать
* Сравнивать полиморф vs полиморф
* (НЕ) Сравнить только куски без библиоткеи
* Сравнить когда и полиморфится sql
* Сделать хорошую прогу, которая дергает sql и сравнить с плохой.
* SSDEEP запустить еще раз.

#### Как запускать fhashes
* TLSH
```
for i in `seq 1 10`; do for k in `seq 1 10`; do python fhash/tlsh_comparator.py binaries/part3/FLA/$i.exe binaries/part3/FLA/$k.exe; done; done > hashes3fla
cat hashes3fla | grep Diffxlen | grep -Po "\d+\.$" | grep -Po "\d+"| grep -v 0 | xargs | sed "s/ /,/g"
```
* Ssdeep
```
./fhash/ssdeep-2.14.1/ssdeep -b binaries/part3/BCF/* > ssdeephashes3bcf
./fhash/ssdeep-2.14.1/ssdeep -b -m ssdeephashes3bcf binaries/part3/BCF/*
```
* Sdhash
```
C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\binaries\mutated_binaries\part3\BCF>C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\fhash\sdhash-4.0-win32\sdhash.exe *.exe > hashes
C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\binaries\mutated_binaries\part3\BCF>C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\fhash\sdhash-4.0-win32\sdhash.exe -c hashes
Linux $ echo $RESULTS > hashes
Linux $ cat hashes | grep -Po "...$" | grep -Po "[1-9]*" | xargs | sed "s/ /,/g"
```