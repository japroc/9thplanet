## TOV 4 - Исследование механизмов полиморфизма программного кода
### Задание
* Для первой лабы реализовать морф, полиморф.
* Представить что антивирус ищется по сигнатуре.
* Написать прогу, которая меняет исхдный код/бинарный код. Но не ручками.
* Можно использовать любые тулзяки пока

### Идея
1. Все строки и константы зашифровать (главное строки)
2. Формат шифрования должен быть особенный.
3. Написать декодер/расшифроватор
4. Написать мутатор кода, пока арифметических операций. Таким образом получится мутировать как код декодера, так и весь код в целом.

### Особенный алгоритм кодирования строк
#### Идея
* При каждой мутации исходника можно гененрировать новое представление строки.
* При каждой мутации исходника меняется код декодера.
* При каждой мутации исходника меняется код в целом.

#### Алгоритм
1. Строка S разбивается на n частей произвольного размера len(s1), ..., len(sn), длиной менее 0xff
2. Каждый блок кодируется с помощью ROT или XOR
3. Алгоритм определяется дополнительным байтом/двумя.
4. Перым байтом в расшифрованной строке являяется \xff, это сигнал, что строка расшифрован до конца и это символ отбрасывается.

#### Прогресс...
смотри тут https://gitlab.com/japroc/9thplanet/tree/master/TOV/TOV4

### На следующий раз
1. Чекнуть компиляцию с оптимизацией кода
2. Добавить аналитики, bindiff и все такое
3. Подумать про изменение потока исполнения
##### Про поток исполнения
1. Внедрение switch-case конструкций на последовательные команды, функции, таким образом последовательный код начинается прыгательным
2. llvm-obfuscator

#### Прогресс
* [llvm-obfuscator wiki](https://github.com/obfuscator-llvm/obfuscator/wiki/Installation)
* [llvm and MS VS](http://llvm.org/docs/GettingStartedVS.html)
* [building llvm on windows issue](https://github.com/obfuscator-llvm/obfuscator/issues/64)
* [PREBUILD BINARIES PAGE](http://releases.llvm.org/download.html)
* [CHINA](https://gitee.com/killvxk/llvm-obfuscator)

##### Compileflow Example
1. Compile to LLVM bytecode
```
clang -I "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\include" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\ucrt" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\um" -c hello.c -emit-llvm -mllvm -bcf -o hello.bc
```
2. Compile to object code
```
llc -filetype=obj hello.bc
```
3. Link using MS linker
```
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\bin\Hostx64\x64\link.exe" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86" /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17763.0\um\x86" /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17763.0\ucrt\x86" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86\store" hello.obj -defaultlib:libcmt -defaultlib:legacy_stdio_definitions
```
4. Как я закомпилил sqlite3.c (НЕ РАБОТАЕТ)
```
cd C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\llvvm-obfuscator-windows\build\Debug\bin
clang.exe -I "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\include" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\ucrt" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\um" -I "C:\Users\japroc\Documents\Visual Studio 2013\Projects\ConsoleApplication1\ConsoleApplication1\backup" -c "C:\Users\japroc\Documents\Visual Studio 2013\Projects\ConsoleApplication1\ConsoleApplication1\backup\sqlite3.c" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\shared" -emit-llvm -o "C:\Users\japroc\Documents\Visual Studio 2013\Projects\ConsoleApplication1\ConsoleApplication1\backup\sqlite3.bc"
clang.exe -I "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\include" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\ucrt" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\um" -I "C:\Users\japroc\Documents\Visual Studio 2013\Projects\ConsoleApplication1\ConsoleApplication1\backup" -c "C:\Users\japroc\Documents\Visual Studio 2013\Projects\ConsoleApplication1\ConsoleApplication1\backup\main.c" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\shared" -emit-llvm -o "C:\Users\japroc\Documents\Visual Studio 2013\Projects\ConsoleApplication1\ConsoleApplication1\backup\main.bc"

cd C:\Users\japroc\Documents\Visual Studio 2013\Projects\ConsoleApplication1\ConsoleApplication1\backup
"C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\llvvm-obfuscator-windows\build\Debug\bin\llc.exe" -filetype=obj main.bc
"C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\llvvm-obfuscator-windows\build\Debug\bin\llc.exe" -filetype=obj sqlite3.bc
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\bin\Hostx64\x64\link.exe" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86" /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17763.0\um\x86" /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17763.0\ucrt\x86" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86\store" main.obj sqlite3.obj -defaultlib:libcmt -defaultlib:legacy_stdio_definitions
main.exe
```

#### Как я это собирал
* Компилим main.c
```
C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\llvvm-obfuscator-windows\build\Debug\bin>clang.exe -I "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\include" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\ucrt" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\um" -I "C:\Users\japroc\source\repos\ConsoleApplication2\ConsoleApplication2\backup" -c "C:\Users\japroc\source\repos\ConsoleApplication2\ConsoleApplication2\backup\main.c" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\shared" -emit-llvm -o "C:\Users\japroc\source\repos\ConsoleApplication2\ConsoleApplication2\backup\main.bc"

"C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\llvvm-obfuscator-windows\build\Debug\bin\llc.exe" -filetype=obj main.bc
```
* Берем sqlite3.obj уже скомпиленный с помощью Visual Studio 2017
* Линкуем
```
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\bin\Hostx64\x64\link.exe" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86" /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17763.0\um\x86" /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17763.0\ucrt\x86" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86\store" main.obj sqlite3.obj

main.exe
```

#### Че дальше?
* Скомпилить мою обфускаловку.
* Скомпилить llvm-обфускаторку.
* Через bindiff и diaphoria(deep hieristics) сравнить.
* сравнить функции через Beyond Compare. (Или другую тулзу сравнения бинарных данных).

#### Последние билды?
```
"C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\llvm-obf-windows\build\Debug\bin\clang.exe" -I "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\include" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\ucrt" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\um" -I "C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\src" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\shared" -c "C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\src\part2\main_part2.c" -emit-llvm -o "C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\binaries\mutated_binaries\part3\build\main.bc"

"C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\llvm-obf-windows\build\Debug\bin\llc.exe" -filetype=obj main.bc

"C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\llvm-obf-windows\build\Debug\bin\clang.exe" -I "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\include" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\ucrt" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\um" -I "C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\src" -I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\shared" -c "C:\Users\japroc\Desktop\9thPlanet\TOV\TOV4\src\sqlite3.c" -emit-llvm -o "C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\binaries\mutated_binaries\part3\build\sqlite3.bc"

"C:\Users\japroc\Desktop\9thPlanet\TOV\TOV5\llvm-obf-windows\build\Debug\bin\llc.exe" -filetype=obj sqlite3.bc

"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\bin\Hostx64\x64\link.exe" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86" /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17763.0\um\x86" /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17763.0\ucrt\x86" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86\store" main.obj sqlite3.obj -defaultlib:legacy_stdio_definitions -defaultlib:libcmt /OUT:main.exe
```

