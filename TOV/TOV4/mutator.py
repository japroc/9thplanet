#!/usr/bin/env python

import re
import os
import sys
import string
import random

def encode_rot(input_):
    output = ''

    str_len = len(input_)
    rot_value = random.randint(1, 255)

    output += chr(str_len) + chr(rot_value)

    for c in input_:
        c_enc = (ord(c) + rot_value) % 256
        output += chr(c_enc)

    return output


def encode_iteration(input_):
    output = ''
    beg_idx = 0

    while (beg_idx != len(input_)):
        length_remained = len(input_) - beg_idx
        max_delim_offset = 255
        
        if (length_remained < max_delim_offset):
            max_delim_offset = length_remained

        delim_position = random.randint(1, max_delim_offset)
        str_to_encode = input_[beg_idx:beg_idx+delim_position]
        output += encode_rot(str_to_encode)

        beg_idx += delim_position

    return output


def encode_str(input_):
    output = "\xff" + input_.replace('\\\\', '\\')
    total_iterations = random.randint(1, 1)
    cnt = 0

    while cnt < total_iterations:
        output = encode_iteration(output)
        cnt += 1

    return output


def bytes_to_binarystr(bytes_):
    return ''.join(['\\x%02x' % ord(b) for b in bytes_])


def help():
    print('Usage: python {} <input_file> <output_file>'.format(sys.argv[0]))


def encode_strings(content):
    output = list()

    pattern = re.compile(r'^(.*?)decode\("(.*?)\"(.*?)$')

    for line in content:

        m = re.match(pattern, line)

        if m:
            out_line = m.group(1) + 'decode("' + bytes_to_binarystr(encode_str(m.group(2)[4:])) + '"' + m.group(3)
        else:
            out_line = line

        output.append(out_line)

    return output


def mutate_integer(value):
    max_int = 1<<32
    safe_int = (1<<16) - 1

    int_value = int(value)
    res = 0
    res_str = ''

    res = seed = random.randint(1, 1000)
    res_str = str(res)

    cnt = 0
    total_mutations = random.randint(1, 10)

    while cnt < total_mutations:
        op = random.randint(1, 3)
        v = random.randint(1, 1000)

        if op == 1 and 0 <= res + v <= safe_int:
            res += v
            res_str += ' + {}'.format(v)

        if op == 2 and 0 <= res - v <= safe_int:
            res -= v
            res_str += ' - {}'.format(v)

        if op == 3 and 0 <= res * v <= safe_int:
            res *= v
            res_str = '(({}) * {})'.format(res_str, v)

        cnt += 1

    if res > int_value:
        diff = res - int_value
        res -= diff
        res_str += ' - {}'.format(diff)

    if res < int_value:
        diff = int_value - res
        res += diff
        res_str += ' + {}'.format(diff)

    return '(' + res_str + ')'


def encode_integers(content):

    output = list()

    pattern = re.compile(r'^(.*?)(\d+)(.*?)$')

    for line in content:

        m = re.match(pattern, line)

        if m and m.group(1)[-1] not in (string.letters + '_'):
            out_line = m.group(1) + mutate_integer(m.group(2)) + m.group(3) 
            # print(out_line)
        else:
            out_line = line

        output.append(out_line)

    return output


def gen_name(length=20):
    return ''.join(random.choice(string.letters) for _ in range(length))


def mutate_integer3(name, value):
    max_int = 1<<32
    safe_int = (1<<16) - 1

    int_value = int(value)
    res = 0
    res_str = 'unsigned int '

    res = seed = random.randint(1, 1000)
    res_str += '{} = {};'.format(name, res)

    cnt = 0
    total_mutations = random.randint(1, 10)

    while cnt < total_mutations:
        op = random.randint(1, 3)
        v = random.randint(1, 1000)

        if op == 1 and 0 <= res + v <= safe_int:
            res += v
            res_str += ' {} += {};'.format(name, v)

        if op == 2 and 0 <= res - v <= safe_int:
            res -= v
            res_str += ' {} -= {};'.format(name, v)

        if op == 3 and 0 <= res * v <= safe_int:
            res *= v
            res_str += ' {} *= {};'.format(name, v)

        cnt += 1

    if res > int_value:
        diff = res - int_value
        res -= diff
        res_str += ' {} -= {}; '.format(name, diff)

    if res < int_value:
        diff = int_value - res
        res += diff
        res_str += ' {} += {}; '.format(name, diff)

    # print('-' * 30)
    # print(res_str)
    # print(value)
    # print('-' * 30)

    return res_str


def encode_integers3(content):

    output = list()

    pattern = re.compile(r'^(.*?)(\-?\d+)(.*?)$')

    for line in content:

        m = re.match(pattern, line)

        if m and m.group(1)[-1] not in (string.letters + '_') and 'char buffer' not in m.group(1):
            tmp_var_name = gen_name()
            out_line = mutate_integer3(tmp_var_name, m.group(2)) + '\r\n'
            out_line += m.group(1) + '(' + tmp_var_name + ')' + m.group(3) 
            print('-' * 30)
            print(line) 
            print(out_line)
            print('-' * 30)

        else:
            out_line = line

        output.append(out_line)

    return output


# def mutate_integer2(line_start, name, value):
#     max_int = 1<<32
#     safe_int = (1<<16) - 1

#     int_value = int(value)
#     res = 0
#     res_str = line_start

#     res = seed = random.randint(1, 1000)
#     res_str += '{} = {};'.format(name, res)

#     cnt = 0
#     total_mutations = random.randint(1, 10)

#     while cnt < total_mutations:
#         op = random.randint(1, 3)
#         v = random.randint(1, 1000)

#         if op == 1 and 0 <= res + v <= safe_int:
#             res += v
#             res_str += ' {} += {};'.format(name, v)

#         if op == 2 and 0 <= res - v <= safe_int:
#             res -= v
#             res_str += ' {} -= {};'.format(name, v)

#         if op == 3 and 0 <= res * v <= safe_int:
#             res *= v
#             res_str += ' {} *= {};'.format(name, v)

#         cnt += 1

#     if res > int_value:
#         diff = res - int_value
#         res -= diff
#         res_str += ' {} -= {};'.format(name, diff)

#     if res < int_value:
#         diff = int_value - res
#         res += diff
#         res_str += ' {} += {};'.format(name, diff)

#     # print(res_str)
#     # print(value)

#     return res_str


# def encode_integers2(content):
#     output = list()

#     pattern = re.compile(r'^(\s+?unsigned int )(.+?)\s?=\s?(\d+?);\s?$')

#     for line in content:

#         m = re.match(pattern, line)

#         if m:
#             out_line = mutate_integer2(m.group(1), m.group(2), m.group(3))
#             print('-' * 30)
#             print(line) 
#             print(out_line)
#             print('-' * 30)
#         else:
#             out_line = line

#         output.append(out_line)

#     return output


def main():
    in_file = sys.argv[1]
    out_file = sys.argv[2]

    if in_file == out_file:
        print('in and out files must be different... exit...')
        exit()

    if not os.path.isfile(in_file):
        print('file not found... exit...')
        exit()

    fp = open(in_file, 'r')
    content = fp.readlines()
    fp.close()

    content2 = encode_strings(content)
    content3 = encode_integers(content2)
    content3 = encode_integers3(content2)

    fp = open(out_file, 'w')
    fp.write(''.join(content3))
    fp.close()

    return


if __name__ == '__main__':
    main() if len(sys.argv) == 3 else help() 
