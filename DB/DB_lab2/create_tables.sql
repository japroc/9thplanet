CREATE TABLE types(
	id AUTO_INCREMENT PRIMARY KEY,
	value VARCHAR(28)
);

CREATE TABLE users(
	name VARCHAR(28) PRIMARY KEY,
	pass_hash CHAR(32),
	type INTEGER NOT NULL CONSTRAINT fk_users_type REFERENCES types (id)
);

CREATE TABLE resources(
	id AUTO_INCREMENT PRIMARY KEY,
	value VARCHAR(28) NOT NULL,
	type INTEGER NOT NULL CONSTRAINT fk_resources_types REFERENCES types (id)
);

CREATE TABLE resource_nesting(
	resource_parent_id INTEGER CONSTRAINT fk_resource_nesting_parent REFERENCES resources (id),
	resource_child_id INTEGER CONSTRAINT fk_resource_nesting_child REFERENCES resources (id),
	PRIMARY KEY (resource_parent_id, resource_child_id)
);

CREATE TABLE access_matrix(
	type_from_id INTEGER CONSTRAINT fk_access_matrix_type_from REFERENCES types (id),
	type_to_id INTEGER CONSTRAINT fk_access_matrix_type_to REFERENCES types (id),
	access_matrix INTEGER NOT NULL,
	PRIMARY KEY (type_from_id, type_to_id)
);
