### Hbase
##### Easy setup
```
git clone https://github.com/dajobe/hbase-docker
cd hbase-docker
sudo ./start-hbase.sh
```
##### Filters
* https://www.cloudera.com/documentation/enterprise/5-7-x/topics/admin_hbase_filtering.html
* How to compare int correctly: https://stackoverflow.com/questions/23249731/filtering-integers-with-hbase-python

### VERTICA
##### Установка docker образа (https://hub.docker.com/r/colemantw/vertica/)
Если кратко:
- docker pull sumitchawla/vertica
- docker run -p 5433:5433 sumitchawla/vertica
- docker run --rm -p 5433:5433 --name db_lab4 sumitchawla/vertica
##### Скачиваем vsql
Если кратко:
- wget —no-check-certificate https://www.vertica.com/client_drivers/9.1.x/9.1.1-0/vertica-client-9.1.1-0.x86_64.tar.gz
- tar xvf vertica-client-9.1.1-0.x86_64.tar.gz
- cd opt/vertica/bin
- ./vsql -h 127.0.0.1 -U dbadmin -d docker

### Лабораторные Работы
[Лаба 2](https://gitlab.com/japroc/9thplanet/tree/master/DB/DB_lab2)

### ПЗ
##### Практическое задание 4.
* Пока мне нефиг делать, вот вам новое задание по БД - страница 166 в методичке, нормализация данных
* Первый пункт просто так наваять от себя, 2-4 по вариантам
* Варианты по списку группы https://vk.com/away.php?to=https%3A%2F%2Fdrive.google.com%2Fdrive%2Ffolders%2F0B1-YfxARhs26NDgyM0lvWGtsclU

### Operations
##### Реляционные исчисления
![pic](https://gitlab.com/japroc/9thplanet/raw/master/DB/RI_Operations.jpg)
##### Реляционная алгебра
* A UNION B *(Объединение)*
* A INTERSECT B *(Пересечение)*
* A MINUS B *(Есть в A, но нет в B)*
* A TIMES B *(Декартово произведение)*
* A PROJECT {a_i} *(Типа SELECT по конкретным полям)*
* A WHERE (condition)
* A JOIN B (condition)
* A(B1, B2) DIVIDE BY C(B1) => K(B2) *(Такие B2, для которых есть все B1)*
* RENAME
* := *(Assignment)*