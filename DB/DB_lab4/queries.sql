CREATE TABLE logs(
    id AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR,
    object VARCHAR,
    access VARCHAR(1),
    decision BOOLEAN,
    access_time TIMESTAMP
);


CREATE OR REPLACE FUNCTION daytots(x INT) RETURN TIMESTAMP
   AS BEGIN 
     RETURN (TIMESTAMPADD (DD, x, (TRUNC(CURRENT_TIMESTAMP, 'MM')))); 
   END;


INSERT INTO logs(username, object, access, decision, access_time) VALUES ('user1', 'obj1', 'r', TRUE, daytots(1));

select row_count, used_bytes, wos_row_count, wos_used_bytes, ros_row_count, ros_used_bytes, ros_count from v_monitor.projection_storage;

select * from storage_containers;