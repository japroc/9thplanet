from __future__ import unicode_literals, absolute_import, print_function

import argparse
import random
from time import time, sleep
import string
from subprocess import Popen, PIPE
from IPython import embed
import traceback
import datetime
import json


MIN_UINT = 0
MAX_UINT = 4294967295
TABLES = ['T1', 'T2']
TABLE = 'logs'
VERTICA_CLIENT_PATH = '/home/japroc/Downloads/opt/vertica/bin/vsql'
VERTICA_CLIENT_ARGS = [VERTICA_CLIENT_PATH, '-h', '127.0.0.1', '-U', 'dbadmin', '-d', 'docker', '-c']


def call_subprocess(args, print_output=False):
    p1 = Popen(args, stdout=PIPE)
    if not print_output:
        return p1.communicate()
    else:
        print(p1.communicate())
        return None


def get_rand_string(length=10):
    return ''.join(random.choice(string.letters) for _ in range(length))


def get_rand_int():
    return random.randint(MIN_UINT, MAX_UINT)


def get_rand_value():
    return "({}, '{}')".format(get_rand_int(), get_rand_string())


def get_rand_bool():
    return random.choice(['TRUE', 'FALSE'])


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--action', required=True, choices=['insert', 'delete', 'create', 'drop', 'info', 'movout', 'calculate1', 'calculate2'] )
    parser.add_argument('-c', '--count',  default=100,   type=int )
    parser.add_argument('-t', '--table',  default='T1' )
    args = parser.parse_args()
    return args


def insert(cnt):

    print_counter = cnt / 10 or 1
    if print_counter > 200:
        print_counter = 200


    i = 0
    while i < cnt:

        diff = 100
        if cnt - i < diff:
            diff = cnt - i

        if i % print_counter == 0:
            print("[=] Counter: {}/{}".format(i, cnt))

        q = "INSERT INTO {table_name}(username, object, access, decision, access_time) {val}; COMMIT;"
        values = ' UNION '.join("SELECT '{}', '{}', '{}', {}, daytots({})".format(
                                get_rand_string(), get_rand_string(), get_rand_string(1), get_rand_bool(), random.randint(0, 2))
                                for _ in range(diff))

        args_command = VERTICA_CLIENT_ARGS + [q.format(table_name=TABLE, val=values)]
        call_subprocess(args_command)

        i += diff


def delete():
    args_command = VERTICA_CLIENT_ARGS + ["DELETE FROM {}; COMMIT;".format(TABLE)]
    call_subprocess(args_command, print_output=True)


def drop():
    args_command = VERTICA_CLIENT_ARGS + ["DROP TABLE {};".format(TABLE)]
    call_subprocess(args_command, print_output=True)


def create():
    q = "CREATE TABLE {}(id AUTO_INCREMENT PRIMARY KEY, username VARCHAR(10), object VARCHAR(10), access VARCHAR(1), decision BOOLEAN, access_time TIMESTAMP);"
    args_command = VERTICA_CLIENT_ARGS + [q.format(TABLE)]
    call_subprocess(args_command, print_output=True)


def get_storage_info():
    q = "select row_count, used_bytes, wos_row_count, wos_used_bytes, ros_row_count, ros_used_bytes, ros_count from v_monitor.projection_storage;"
    args_command = VERTICA_CLIENT_ARGS + [q.format(TABLE)]
    res = call_subprocess(args_command)
    parts = res[0].split()
    
    try:
        res_dict =  {
            'row_count': int(parts[14]),
            'used_bytes': int(parts[16]),
            'wos_row_count': int(parts[18]),
            'wos_used_bytes': int(parts[20]),
            'ros_row_count': int(parts[22]),
            'ros_used_bytes': int(parts[24]),
            'ros_count': int(parts[26])
        }
        return res_dict

    except ValueError:
        res_dict =  {
            'row_count': 0,
            'used_bytes': 0,
            'wos_row_count': 0,
            'wos_used_bytes': 0,
            'ros_row_count': 0,
            'ros_used_bytes': 0,
            'ros_count': 0
        }
        return res_dict


def get_only_rosed_storage_info():
    while True:
        info = get_storage_info()
        if info['wos_row_count'] == 0:
            return info
    

def log_str(string):
    fp = open('results.txt', 'a')
    fp.write(string)
    fp.close()


def set_movout_period(sec=120):
    q = "SELECT set_config_parameter('MoveOutInterval',{});".format(sec)
    args_command = VERTICA_CLIENT_ARGS + [q.format(TABLE)]
    call_subprocess(args_command, print_output=True)


def calculate1():
    fp = open('results.txt', 'w')
    fp.write('{}\n'.format(datetime.datetime.now()))
    fp.close()

    drop()
    create()

    try:
        for i in range(1000):
            insert(1)
            log_str(json.dumps(get_storage_info()) + '\n')
            log_str(json.dumps(get_only_rosed_storage_info()) + '\n')
    except Exception as e:
        traceback.print_exc()


def calculate2():
    fp = open('results.txt', 'w')
    fp.write('{}\n'.format(datetime.datetime.now()))
    fp.close()

    drop()
    create()
    sleep(3)
    set_movout_period(60 * 60)

    try:
        for i in range(1000):
            insert(1)
            log_str(json.dumps(get_storage_info()) + '\n')
    except Exception as e:
        traceback.print_exc()


if __name__ == '__main__':
    start_time = time()
    args = parse_args()

    if args.action == 'insert':
        insert(args.count)
    if args.action == 'movout':
        set_movout_period(args.count)
    if args.action == 'delete':
        delete()
    if args.action == 'drop':
        drop()
    if args.action == 'create':
        create()
    if args.action == 'info':
        get_storage_info()
    if args.action == 'calculate1':
        calculate1()
    if args.action == 'calculate2':
        calculate2()

    finish_time = time()
    print('[~] Finished in {} seconds'.format(finish_time - start_time))
